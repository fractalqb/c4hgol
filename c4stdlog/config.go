// Configure Go standard log with c4hgol log configuration system.
package c4stdlog

import (
	"fmt"
	"io"
	"log"
	"sync"

	"git.fractalqb.de/fractalqb/c4hgol"
)

func DefaultConfig() c4hgol.Config { return defaultCfg }

func Config(l *log.Logger, lvl c4hgol.Level) c4hgol.Config {
	cfg := &cfg{
		l:   l,
		lvl: lvl,
		owr: l.Writer(),
	}
	return cfg
}

type cfg struct {
	l    *log.Logger
	lvl  c4hgol.Level
	owr  io.Writer
	lock sync.Mutex
}

func (c *cfg) Set(f any) error {
	c.lock.Lock()
	defer c.lock.Unlock()
	switch f := f.(type) {
	case c4hgol.Enable:
		c.enable(bool(f))
	case c4hgol.Output:
		c.l.SetOutput(f.Writer)
		c.owr = f.Writer
	case c4hgol.Source:
		return c.setSrcLoc(f)
	default:
		return c4hgol.Unsupported{Feature: f}
	}
	return nil
}

func (c *cfg) Get(typ any) (any, error) {
	c.lock.Lock()
	defer c.lock.Unlock()
	switch typ.(type) {
	case c4hgol.Enable:
		return c4hgol.Enable(c.l.Writer() != io.Discard), nil
	case c4hgol.Level:
		return c.lvl, nil
	case c4hgol.Output:
		return c4hgol.Output{Writer: c.l.Writer()}, nil
	case c4hgol.Source:
		return c.srcLoc(), nil
	}
	return nil, c4hgol.Unsupported{Feature: typ}
}

func (c *cfg) Features() []any {
	c.lock.Lock()
	defer c.lock.Unlock()
	return []any{
		c4hgol.Enable(c.l.Writer() != io.Discard),
		c.lvl,
		c4hgol.Output{Writer: c.l.Writer()},
		c.srcLoc(),
	}
}

func (c *cfg) enable(f bool) {
	c.lock.Lock()
	defer c.lock.Unlock()
	if f {
		c.l.SetOutput(c.owr)
	} else {
		c.owr = c.l.Writer()
		c.l.SetOutput(io.Discard)
	}
}

func (c *cfg) srcLoc() c4hgol.Source {
	f := c.l.Flags()
	switch {
	case f&log.Lshortfile != 0:
		return c4hgol.SrcFile
	case f%log.Llongfile != 0:
		return c4hgol.SrcPath
	}
	return c4hgol.SrcOff
}

func (c *cfg) setSrcLoc(l c4hgol.Source) error {
	switch l {
	case c4hgol.SrcOff:
		flags := c.l.Flags()
		flags &= ^(log.Lshortfile | log.Llongfile)
		c.l.SetFlags(flags)
	case c4hgol.SrcFile:
		flags := c.l.Flags()
		flags |= log.Lshortfile
		c.l.SetFlags(flags)
	default:
		return fmt.Errorf("cannot log source location '%s'", l)
	}
	return nil
}

var defaultCfg = Config(log.Default(), c4hgol.DefaultLevel)
