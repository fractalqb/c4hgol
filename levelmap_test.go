package c4hgol

import (
	"testing"

	"git.fractalqb.de/fractalqb/catch/test"
)

func TestLevelMap(t *testing.T) {
	m := test.Err(NewLevelMap(
		-100, -10,
		0, 0,
		100, 10,
	)).ShallNot(t)
	t.Run("forward", func(t *testing.T) {
		check := func(from, to int64) {
			mto := m.Map(from)
			if to != mto {
				t.Fatalf("%d -> expect %d, got %d", from, to, mto)
			}
		}
		check(-110, -11)
		check(-100, -10)
		check(-50, -5)
		check(0, 0)
		check(50, 5)
		check(100, 10)
		check(110, 11)
	})
	t.Run("backward", func(t *testing.T) {
		check := func(from, to int64) {
			mto := m.Revert(from)
			if to != mto {
				t.Fatalf("%d -> expect %d, got %d", from, to, mto)
			}
		}
		check(-11, -110)
		check(-10, -100)
		check(-5, -50)
		check(0, 00)
		check(5, 50)
		check(10, 100)
		check(11, 110)
	})
}
