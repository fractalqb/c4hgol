package c4hgol

import (
	"fmt"
	"io"
)

type Enable bool

type Level int

func (l Level) String() string {
	switch l {
	case TraceLevel:
		return "trace"
	case DebugLevel:
		return "debug"
	case InfoLevel:
		return "info"
	case WarnLevel:
		return "warn"
	case ErrorLevel:
		return "error"
	}
	return fmt.Sprintf("%s=%d", FeatureType(l), int(l))
}

const (
	TraceLevel Level = -100
	DebugLevel Level = -50
	InfoLevel  Level = 0
	WarnLevel  Level = 50
	ErrorLevel Level = 100

	DefaultLevel = InfoLevel
)

type Output struct {
	io.Writer
}

func (o Output) String() string {
	if o.Writer == nil {
		return fmt.Sprintf("%s=<nil>", FeatureType(o))
	}
	return fmt.Sprintf("%s=%T", FeatureType(o), o.Writer)
}

type Source int

func (l Source) String() string {
	switch l {
	case SrcOff:
		return "off"
	case SrcFile:
		return "file"
	case SrcPath:
		return "path"
	case SrcStack:
		return "stack"
	}
	return fmt.Sprintf("%s=<invalid:%d>", FeatureType(l), int(l))
}

const (
	SrcOff Source = iota
	SrcFile
	SrcPath
	SrcStack
)
