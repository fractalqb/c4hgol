package c4hgol

import (
	"fmt"
	"reflect"
)

type Config interface {
	Set(feature any) error
	Get(zero any) (value any, err error)
	Features() []any
}

type Unsupported struct {
	Feature any
}

func (u Unsupported) Is(err error) bool {
	_, ok := err.(Unsupported)
	return ok
}

func (u Unsupported) Error() string {
	return fmt.Sprintf("unsupported feature '%s'", FeatureType(u.Feature))
}

func FeatureType(feature any) string {
	if feature == nil {
		return ""
	}
	if f, ok := feature.(interface{ Type() string }); ok {
		return f.Type()
	}
	t := reflect.Indirect(reflect.ValueOf(feature)).Type()
	return t.PkgPath() + "." + t.Name()
}
