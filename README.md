# c4hgol

[![Test Coverage](https://img.shields.io/badge/coverage-20%25-red.svg)](file:coverage.html)
[![Go Report Card](https://goreportcard.com/badge/codeberg.org/fractalqb/c4hgol)](https://goreportcard.com/report/codeberg.org/fractalqb/c4hgol)
[![GoDoc](https://godoc.org/codeberg.org/fractalqb/c4hgol?status.svg)](https://pkg.go.dev/git.fractalqb.de/fractalqb/c4hgol)

`import "git.fractalqb.de/fractalqb/c4hgol"`

---

# Configuration for Heterogeneous Go Loggers

Though Go has a logging package in its standard library this package
is rather basic. IMHO this is the reason why there are several
additional and different logging libraries in the Go community which
lead to some irritation when one wants to write an application that
ends up with having several different logging frameworks included due
to package dependencies.

This package does _not_ address the desire to have one unified logger
or at least a unified logging interface. – Let the packages choose
their loggers… for good or for bad. However, what I want to be able to
do is configure all those loggers in my application in a unified way!
At least configure some common things that one often wants to
configure:

  * Switching loggers for some “topics” on and off
  * Setting the log level of loggers
  * Set a writer as the target for the logging messages
  * (to come: log format?)

Note that the first bullet in the list gives the hint that loggers
shall be grouped into *topics* so that configuration becomes simpler.

## Log Packages Architecture Proposition

Make a virtue of necessity…

### Theoretical Approach

First of all one has to question why it could come to the situation that a
single executable includes different logging libraries! If each component
(library), an executable is made of, returns detailed information about the
outcome of each of its offered services (functions/methods) the executable can
decide if and how to log those outcomes.—No need for a component to do logging
at all, i.e. only the executable uses logging and therefore can arrange to use
one logging library.

In theory this could be done for _exceptional_ situations, aka errors. Here one
could start to collect context information at the point of failure. This
information then propagates back to the executable while being enriched with
relevant context information. Again, it would be up to the executable to decide
what to do with this.

For “small” components and when executables have a rather shallow
dependency graph this approach does not only work well, it is also a
desirable approach. – It keeps things simple!

### Facing the Real World…
…at least when things become more and more complex. In reality we
often see requirements that break the Theoretical Approach:

- In practice error/exception handling is rarely perfect and most
  often only comes with a text message. This makes reasonable
  decisions difficult for executables.

- As soon as one also wants information about the happy case details
  from a deeply “buried” component it gets difficult, if not
  impossible, for top-level executable's code to get that.

For short: _One has to expect that sufficiently sophisticated
components come with their own logging included._

Would that be a problem? IMHO not, as long as you have some means to
configure the loggers to your need. I.e. make _c4hgol_ a concept! This
would address the need of application developers and gives reasonable
choices to component developers:

1. Component developers can chose their logging API

2. Logging calls can be static calls, i.e. no dynamic dispatching
   through interfaces

## Used by
- [qbsllm](https://github.com/fractalqb/qblog) – combines [sllm structured
  logging](https://github.com/fractalqb/sllm) and c4hgol into one logging library.
