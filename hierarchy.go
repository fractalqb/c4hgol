package c4hgol

import (
	"errors"
	"fmt"
	"path/filepath"
	"strconv"
	"strings"
	"sync/atomic"
)

type Named struct {
	parent atomic.Value
	cfg    Config
	name   string
}

func NewNamed(cfg Config, name string) (*Named, error) {
	if cfg == nil {
		return nil, errors.New("naming nil Config")
	}
	switch {
	case name == "":
		return nil, errors.New("empty name")
	case strings.ContainsRune(name, '/'):
		return nil, fmt.Errorf("name '%s' contains separator '/'", name)
	}
	return &Named{cfg: cfg, name: name}, nil
}

func MustNewNamed(cfg Config, name string) *Named {
	n, err := NewNamed(cfg, name)
	if err != nil {
		panic(err)
	}
	return n
}

func (n *Named) Name() string { return n.name }

func (n *Named) Path() string {
	if p := n.parent.Load(); p != nil {
		return p.(*Group).Path() + "/" + n.name
	}
	return "/" + n.name
}

func (n *Named) Set(feature any) error               { return n.cfg.Set(feature) }
func (n *Named) Get(zero any) (value any, err error) { return n.cfg.Get(zero) }
func (n *Named) Features() []any                     { return n.cfg.Features() }

func (n *Named) setParent(g *Group) error {
	if !n.parent.CompareAndSwap(nil, g) {
		return fmt.Errorf("cannot re-add %s to %s", n.Path(), g.Path())
	}
	return nil
}

type Group struct {
	Named
	members []Config
}

func NewGroup(name string, members ...Config) (*Group, error) {
	g := &Group{Named: Named{name: name}, members: members}
	for _, m := range members {
		switch m := m.(type) {
		case *Named:
			if err := m.setParent(g); err != nil {
				return nil, err
			}
		case *Group:
			if err := m.setParent(g); err != nil {
				return nil, err
			}
		default:
			return nil, fmt.Errorf("group members must be Named or Group, got %T", m)
		}
	}
	return g, nil
}

func MustNewGroup(name string, members ...Config) *Group {
	g, err := NewGroup(name, members...)
	if err != nil {
		panic(err)
	}
	return g
}

func NewLogGroup(cfg Config, name string, members ...Config) (*Group, error) {
	g, err := NewGroup(name, members...)
	if err != nil {
		return nil, err
	}
	g.cfg = cfg
	return g, nil
}

func MustNewLogGroup(cfg Config, name string, members ...Config) *Group {
	g, err := NewLogGroup(cfg, name, members...)
	if err != nil {
		panic(err)
	}
	return g
}

func (g *Group) Set(feature any) (err error) {
	if g.cfg != nil {
		err = g.cfg.Set(feature)
		switch {
		case errors.Is(err, Unsupported{}):
			err = fmt.Errorf("unsupported by %s: %w", g.Path(), err)
		case err != nil:
			return err
		}
	}
	for _, m := range g.members {
		e := m.Set(feature)
		switch {
		case errors.Is(e, Unsupported{}):
			if err == nil {
				err = e
			}
		case err != nil:
			return e
		}
	}
	return err
}

func Visit(start Config, do func(c Config, path string) error) error {
	switch start := start.(type) {
	case *Named:
		return do(start.cfg, start.Path())
	case *Group:
		return doVisit(start, start.Path(), do)
	}
	return do(start, "")
}

func doVisit(g *Group, p string, do func(Config, string) error) error {
	if g.cfg != nil {
		if err := do(g.cfg, p); err != nil {
			if !errors.Is(err, Unsupported{}) {
				return err
			}
		}
	}
	for i, m := range g.members {
		switch m := m.(type) {
		case *Named:
			if err := do(m.cfg, filepath.Join(p, m.name)); err != nil {
				if !errors.Is(err, Unsupported{}) {
					return err
				}
			}
		case *Group:
			if err := doVisit(g, filepath.Join(p, g.name), do); err != nil {
				if !errors.Is(err, Unsupported{}) {
					return err
				}
			}
		default:
			ep := filepath.Join(p, strconv.Itoa(i+1))
			if err := do(m, ep); err != nil {
				if !errors.Is(err, Unsupported{}) {
					return err
				}
			}
		}
	}
	return nil
}
