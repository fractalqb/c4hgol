package c4hgol

import (
	"fmt"
	"path"
	"path/filepath"
	"regexp"
	"strconv"
	"strings"
)

var termParse = regexp.MustCompile(
	`^(?:([ewidt])|(-?[0-9]+))(\+[fps])?(` +
		string(filepath.ListSeparator) +
		`.+)?$`,
)

func configureLevel(cfg Config, l Level) error {
	err := cfg.Set(l)
	if err == nil {
		return nil
	}
	cl, err := cfg.Get(DefaultLevel)
	if err != nil {
		return err
	}
	err = cfg.Set(Enable(cl.(Level) >= l))
	if err != nil {
		return fmt.Errorf("cannot configure level: %w", err)
	}
	return nil
}

// TODO enable / disable
func Configure(cfg Config, desc string, strict bool) error {
	const (
		lname = 1 + iota
		lnum
		lsrc
		lpath
	)
	if desc == "" {
		return nil
	}
	terms := strings.Split(desc, ",")
	for _, t := range terms {
		match := termParse.FindStringSubmatch(t)
		if match == nil {
			return fmt.Errorf("syntax error in configuration term '%s'", t)
		}
		var lvl Level
		if match[lname] != "" {
			switch match[lname][0] { // only ASCII characters
			case 't':
				lvl = TraceLevel
			case 'd':
				lvl = DebugLevel
			case 'i':
				lvl = InfoLevel
			case 'w':
				lvl = WarnLevel
			case 'e':
				lvl = ErrorLevel
			}
		} else {
			i, err := strconv.Atoi(match[lnum])
			if err != nil {
				return err
			}
			lvl = Level(i)
		}
		var pattern string
		if match[lpath] != "" {
			pattern = match[lpath][1:]
		}
		err := Visit(cfg, func(cfg Config, p string) error {
			if pattern != "" {
				if ok, err := path.Match(pattern, p); err != nil && strict {
					return err
				} else if !ok {
					return nil
				}
			}
			err := configureLevel(cfg, lvl)
			if err != nil && strict {
				return err
			}
			if match[lsrc] == "" {
				if err = cfg.Set(SrcOff); err != nil && strict {
					return err
				}
			} else {
				switch match[lsrc][1] {
				case 'f':
					if err = cfg.Set(SrcFile); err != nil && strict {
						return err
					}
				case 'p':
					if err = cfg.Set(SrcPath); err != nil && strict {
						return err
					}
				case 's':
					if err = cfg.Set(SrcStack); err != nil && strict {
						return err
					}
				}
			}
			return nil
		})
		if err != nil {
			return err
		}
	}
	return nil
}

func FlagDoc() string {
	return fmt.Sprintf(`Set logging verbosity <term>[,<term>…]
  <term> is <level>[+{fps}][:<glob>]
The standard log leves can be chosen with their name. Each name
represents a numerical value. One can choose log levels in between by
directly giving such a number.
The standard log level are:
 - e: error (%4d)
 - w: warn  (%4d)
 - i: info  (%4d)
 - d: debug (%4d)
 - t: trace (%4d)`,
		ErrorLevel, WarnLevel, InfoLevel, DebugLevel, TraceLevel,
	)
}
