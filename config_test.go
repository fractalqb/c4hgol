package c4hgol

import (
	"fmt"
	"os"
)

func ExampleFeatureType() {
	fmt.Println(FeatureType(Enable(false)))
	fmt.Println(FeatureType(InfoLevel))
	fmt.Println(FeatureType(Output{os.Stderr}))
	fmt.Println(FeatureType(SrcFile))
	// Output:
	// git.fractalqb.de/fractalqb/c4hgol.Enable
	// git.fractalqb.de/fractalqb/c4hgol.Level
	// git.fractalqb.de/fractalqb/c4hgol.Output
	// git.fractalqb.de/fractalqb/c4hgol.Source
}
