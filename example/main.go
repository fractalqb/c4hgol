package main

import (
	"fmt"
	"log"
	"os"

	"git.fractalqb.de/fractalqb/c4hgol"
	"git.fractalqb.de/fractalqb/c4hgol/c4stdlog"
)

var (
	warn = log.New(os.Stderr, "c4hgol ", log.LstdFlags)

	cfg = c4hgol.MustNewLogGroup(c4stdlog.DefaultConfig(), "app",
		c4hgol.MustNewNamed(c4stdlog.Config(log.Default(), c4hgol.InfoLevel), "info"),
		c4hgol.MustNewNamed(c4stdlog.Config(warn, c4hgol.WarnLevel), "warn"),
	)
)

func main() {
	log.Print("Info message")
	warn.Print("Waring message")
	c4hgol.Visit(cfg, func(c c4hgol.Config, p string) error {
		fmt.Printf("%s: %T = %+v\n", p, c, c.Features())
		return nil
	})
}
