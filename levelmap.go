package c4hgol

import (
	"errors"
	"fmt"
	"math"
	"reflect"
	"slices"
)

type LevelMap struct {
	as []float64
	bs []float64
}

func NewLevelMap(pairs ...any) (LevelMap, error) {
	l := len(pairs)
	switch {
	case l == 0:
		return LevelMap{}, errors.New("empty level map")
	case l%2 == 1:
		return LevelMap{}, errors.New("odd number of levels")
	}
	l2 := l / 2
	m := LevelMap{as: make([]float64, l)}
	m.bs = m.as[l2:]
	m.as = m.as[:l2]
	var err error
	for i := 0; i < l2; i++ {
		m.as[i], err = lmapToF64(pairs[2*i])
		if err != nil {
			return LevelMap{}, err
		}
		m.bs[i], err = lmapToF64(pairs[2*i+1])
		if err != nil {
			return LevelMap{}, err
		}
	}
	if !slices.IsSorted(m.as) {
		return LevelMap{}, errors.New("source levels not sorted")
	}
	if !slices.IsSorted(m.bs) {
		return LevelMap{}, errors.New("destination levels not sorted")
	}
	return m, nil
}

func MustNewLevelMap(pairs ...any) LevelMap {
	m, err := NewLevelMap(pairs...)
	if err != nil {
		panic(err)
	}
	return m
}

func (m LevelMap) Map(a int64) int64 { return lmap(m.as, m.bs, a) }

func (m LevelMap) Revert(b int64) int64 { return lmap(m.bs, m.as, b) }

func lmap(as, bs []float64, a int64) int64 {
	f := float64(a)
	idx, hit := slices.BinarySearch(as, f)
	if hit {
		return int64(bs[idx])
	}
	if idx == 0 {
		g := bs[0] * f / as[0]
		return int64(g)
	} else if l := len(as); idx == l {
		l--
		g := bs[l] * f / as[l]
		return int64(g)
	}
	r := (f - as[idx-1]) / (as[idx] - as[idx-1])
	g := bs[idx-1] + r*(bs[idx]-bs[idx-1])
	return int64(math.Round(g))
}

func lmapToF64(l any) (float64, error) {
	v := reflect.ValueOf(l)
	if !v.CanInt() {
		return 0, fmt.Errorf("invalid log-level %[1]T=%[1]v", l)
	}
	return float64(v.Int()), nil
}
