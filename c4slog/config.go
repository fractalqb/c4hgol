package c4slog

import (
	"log/slog"

	"git.fractalqb.de/fractalqb/c4hgol"
)

type HandlerOptions slog.HandlerOptions

func (cfg *HandlerOptions) Set(f any) error {
	switch f := f.(type) {
	case c4hgol.Level:
		if lv, ok := cfg.Level.(*slog.LevelVar); ok {
			lv.Set(slog.Level(lmap.Map(int64(f))))
		} else {
			return c4hgol.Unsupported{Feature: f}
		}
	case c4hgol.Source:
		cfg.AddSource = f != c4hgol.SrcOff
	default:
		return c4hgol.Unsupported{Feature: f}
	}
	return nil
}

func (cfg HandlerOptions) Get(typ any) (any, error) {
	switch typ.(type) {
	case c4hgol.Level:
		if cfg.Level != nil {
			l := lmap.Revert(int64(cfg.Level.Level()))
			return slog.Level(l), nil
		}
	case c4hgol.Source:
		if slog.HandlerOptions(cfg).AddSource {
			return c4hgol.SrcPath, nil
		}
		return c4hgol.SrcOff, nil
	}
	return nil, c4hgol.Unsupported{Feature: typ}
}

func (cfg HandlerOptions) Features() []any {
	level, _ := cfg.Get(c4hgol.DefaultLevel)
	return []any{level}
}

var lmap = c4hgol.MustNewLevelMap(
	c4hgol.TraceLevel, 2*slog.LevelDebug,
	c4hgol.DebugLevel, slog.LevelDebug,
	c4hgol.InfoLevel, slog.LevelInfo,
	c4hgol.WarnLevel, slog.LevelWarn,
	c4hgol.ErrorLevel, slog.LevelError,
)
