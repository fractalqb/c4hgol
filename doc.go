// Though Go has a logging package in its standard library this package
// is rather basic. IMHO this is the reason why there are several
// additional and different logging libraries in the Go community which
// lead to some irritation when one wants to write an application that
// ends up with having several different logging frameworks included due
// to package dependencies.
//
// This package does not address the desire to have one unified logger or
// at least a unified logging interface. – Let the packages choose their
// loggers… for good or for bad. However, what I want to be able to do is
// configure all those loggers in my application in a unified way! At
// least configure some common things that one often wants to configure:
//
// - Switching loggers for some “topics” on and off
//
// - Setting the log level of loggers
//
// - (may be other things to come in the future)
//
// Note that the first bullet in the list gives the hint that loggers
// shall be hierarchically grouped so that configuration becomes simpler.
package c4hgol
